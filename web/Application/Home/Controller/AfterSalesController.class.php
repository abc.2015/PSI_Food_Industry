<?php

namespace Home\Controller;

use Home\Common\FIdConst;
use Home\Service\UserService;
use Think\Controller;
use Home\Service\ASBillService;

/**
 * 售后 Controller
 *
 * @author 李静波
 *        
 */
class AfterSalesController extends PSIBaseController {

	/**
	 * 售后服务 - 主页面
	 */
	public function index() {
		$us = new UserService();
		
		if ($us->hasPermission(FIdConst::AFTER_SALES)) {
			$this->initVar();
			
			$this->assign("title", "售后服务");
			
			$this->display();
		} else {
			$this->gotoLoginPage("/Home/AfterSales/index");
		}
	}

	public function asbillList() {
		if (IS_POST) {
			$params = array(
					"billStatus" => I("post.billStatus"),
					"ref" => I("post.ref"),
					"fromDT" => I("post.fromDT"),
					"toDT" => I("post.toDT"),
					"warehouseId" => I("post.warehouseId"),
					"customerId" => I("post.customerId"),
					"bizType" => I("post.bizType"),
					"page" => I("post.page"),
					"start" => I("post.start"),
					"limit" => I("post.limit")
			);
			
			$as = new ASBillService();
			$this->ajaxReturn($as->asbillList($params));
		}
	}

	public function asBillInfo() {
		if (IS_POST) {
			$params = array(
					"id" => I("post.id")
			);
			
			$as = new ASBillService();
			$this->ajaxReturn($as->asBillInfo($params));
		}
	}

	public function selectSOBillList() {
		if (IS_POST) {
			$params = array(
					"ref" => I("post.ref"),
					"customerId" => I("post.customerId"),
					"warehouseId" => I("post.warehouseId"),
					"fromDT" => I("post.fromDT"),
					"toDT" => I("post.toDT"),
					"page" => I("post.page"),
					"start" => I("post.start"),
					"limit" => I("post.limit")
			);
			
			$as = new ASBillService();
			$this->ajaxReturn($as->selectSOBillList($params));
		}
	}

	public function getSOBillInfoForASBill() {
		if (IS_POST) {
			$params = array(
					"id" => I("post.id")
			);
			
			$as = new ASBillService();
			$this->ajaxReturn($as->getSOBillInfoForASBill($params));
		}
	}

	/**
	 * 新建或编辑售后单
	 */
	public function editASBill() {
		if (IS_POST) {
			$params = array(
					"jsonStr" => I("post.jsonStr")
			);
			
			$as = new ASBillService();
			$this->ajaxReturn($as->editASBill($params));
		}
	}

	/**
	 * 退货明细
	 */
	public function asBillRejDetailList() {
		if (IS_POST) {
			$params = array(
					"id" => I("post.billId")
			);
			
			$as = new ASBillService();
			$this->ajaxReturn($as->asBillRejDetailList($params));
		}
	}

	/**
	 * 换货、补发明细
	 */
	public function asBillExDetailList() {
		if (IS_POST) {
			$params = array(
					"id" => I("post.billId")
			);
			
			$as = new ASBillService();
			$this->ajaxReturn($as->asBillExDetailList($params));
		}
	}

	/**
	 * 删除售后单
	 */
	public function deleteASBill() {
		if (IS_POST) {
			$params = array(
					"id" => I("post.id")
			);
			
			$as = new ASBillService();
			$this->ajaxReturn($as->deleteASBill($params));
		}
	}

	/**
	 * 审核售后单
	 */
	public function commitASBill() {
		if (IS_POST) {
			$params = array(
					"id" => I("post.id")
			);
			
			$as = new ASBillService();
			$this->ajaxReturn($as->commitASBill($params));
		}
	}

	/**
	 * 取消审核
	 */
	public function cancelCommitASBill() {
		if (IS_POST) {
			$params = array(
					"id" => I("post.id")
			);
			
			$as = new ASBillService();
			$this->ajaxReturn($as->cancelCommitASBill($params));
		}
	}
}