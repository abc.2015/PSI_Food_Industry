<?php

namespace Home\Service;

/**
 * 条码库 Service
 *
 * @author 李静波
 */
class BarcodeService extends PSIBaseService {

	public function editBarcode($params) {
		if ($this->isNotOnline()) {
			return $this->notOnlineError();
		}
		
		$id = $params["id"];
		$code = $params["code"];
		
		$db = M();
		$db->startTrans();
		
		if ($id) {
			// 编辑
			
			// 检查条码是否已经存在
			$sql = "select count(*) as cnt from t_barcode 
					where barcode = '%s' and id <> '%s' ";
			$data = $db->query($sql, $code, $id);
			$cnt = $data[0]["cnt"];
			if ($cnt > 0) {
				$db->rollback();
				return $this->bad("条码[{$code}]已经存在");
			}
			
			$sql = "update t_barcode
					set barcode = '%s'
					where id = '%s' ";
			$rc = $db->execute($sql, $code, $id);
			if ($rc === false) {
				$db->rollback();
				return $this->sqlError(__LINE__);
			}
		} else {
			// 新建条码
			
			// 检查条码是否已经存在
			$sql = "select count(*) as cnt from t_barcode where barcode = '%s' ";
			$data = $db->query($sql, $code);
			$cnt = $data[0]["cnt"];
			if ($cnt > 0) {
				$db->rollback();
				return $this->bad("条码[{$code}]已经存在");
			}
			
			$idGen = new IdGenService();
			$id = $idGen->newId($db);
			
			$sql = "insert into t_barcode (id, barcode)
					values ('%s', '%s')";
			$rc = $db->execute($sql, $id, $code);
			if ($rc === false) {
				$db->rollback();
				return $this->sqlError(__LINE__);
			}
		}
		
		$db->commit();
		
		return $this->ok($id);
	}

	public function barcodeList($params) {
		if ($this->isNotOnline()) {
			return $this->emptyResult();
		}
		
		$barcode = $params["barcode"];
		$goodsId = $params["goodsId"];
		
		$page = $params["page"];
		$start = $params["start"];
		$limit = $params["limit"];
		
		$result = array();
		
		$db = M();
		$queryParams = array();
		
		$sql = "select id, barcode
				from t_barcode
				where (1 = 1) ";
		
		if ($barcode) {
			$sql .= " and (barcode like '%s' ) ";
			$queryParams[] = "%{$barcode}%";
		}
		
		if ($goodsId) {
			$sql .= " and id in (select barcode_id from t_goods_barcode where goods_id = '%s')";
			$queryParams[] = $goodsId;
		}
		
		$sql .= " order by barcode
				  limit %d, %d";
		$queryParams[] = $start;
		$queryParams[] = $limit;
		
		$data = $db->query($sql, $queryParams);
		foreach ( $data as $v ) {
			$item = array(
					"id" => $v["id"],
					"barcode" => $v["barcode"]
			);
			
			$result[] = $item;
		}
		
		$queryParams = array();
		$sql = "select count(*) as cnt from t_barcode
				where (1 = 1)";
		if ($barcode) {
			$sql .= " and (barcode like '%s' ) ";
			$queryParams[] = "%{$barcode}%";
		}
		
		if ($goodsId) {
			$sql .= " and id in (select barcode_id from t_goods_barcode where goods_id = '%s')";
			$queryParams[] = $goodsId;
		}
		$data = $db->query($sql, $queryParams);
		$cnt = $data[0]["cnt"];
		
		return array(
				"dataList" => $result,
				"totalCount" => $cnt
		);
	}

	public function goodsList($params) {
		if ($this->isNotOnline()) {
			return $this->emptyResult();
		}
		
		$id = $params["id"];
		
		$result = array();
		
		$db = M();
		
		$sql = "select g.code, g.name, g.spec
				from t_goods_barcode b, t_goods g
				where b.goods_id = g.id and b.barcode_id = '%s' 
				order by g.code";
		$data = $db->query($sql, $id);
		foreach ( $data as $v ) {
			$item = array(
					"goodsCode" => $v["code"],
					"goodsName" => $v["name"],
					"goodsSpec" => $v["spec"]
			);
			
			$result[] = $item;
		}
		
		return $result;
	}

	public function deleteBarcode($params) {
		if ($this->isNotOnline()) {
			return $this->notOnlineError();
		}
		
		$id = $params["id"];
		
		$db = M();
		
		$db->startTrans();
		
		$sql = "select barcode from t_barcode where id = '%s' ";
		$data = $db->query($sql, $id);
		if (! $data) {
			$db->rollback();
			return $this->bad("要删除的条码不存在");
		}
		$barcode = $data[0]["barcode"];
		
		$sql = "select count(*) as cnt 
				from t_goods_barcode 
				where barcode_id = '%s' ";
		$data = $db->query($sql, $id);
		$cnt = $data[0]["cnt"];
		if ($cnt > 0) {
			$db->rollback();
			return $this->bad("条码[{$barcode}]已被使用，不能删除");
		}
		
		$sql = "delete from t_barcode where id = '%s' ";
		$rc = $db->execute($sql, $id);
		if ($rc === false) {
			$db->rollback();
			return $this->sqlError(__LINE__);
		}
		
		$db->commit();
		
		return $this->ok();
	}

	public function queryData($queryKey) {
		if ($this->isNotOnline()) {
			return $this->emptyResult();
		}
		
		if ($queryKey == null) {
			$queryKey = "";
		}
		
		$key = "%{$queryKey}%";
		
		$sql = "select id, barcode
				from t_barcode
				where (barcode like '%s') ";
		$queryParams = array();
		$queryParams[] = $key;
		
		$sql .= " order by barcode
				limit 20";
		$data = M()->query($sql, $queryParams);
		$result = array();
		foreach ( $data as $i => $v ) {
			$item = array(
					"id" => $v["id"],
					"barcode" => $v["barcode"]
			);
			
			$result[] = $item;
		}
		
		return $result;
	}
}