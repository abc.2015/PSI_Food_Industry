<?php

namespace Home\Service;

use Home\DAO\TaobaoShopDAO;

/**
 * 淘宝Service
 *
 * @author 李静波
 */
class TaobaoService extends PSIBaseService {

	/**
	 * 淘宝店铺列表
	 */
	public function shopList() {
		if ($this->isNotOnline()) {
			return $this->emptyResult();
		}
		
		$td = new TaobaoShopDAO();
		
		return $td->shopList();
	}

	/**
	 * 新增或编辑淘宝店铺
	 */
	public function editShop($params) {
		if ($this->isNotOnline()) {
			return $this->notOnlineError();
		}
		
		$td = new TaobaoShopDAO();
		return $td->editShop($params);
	}

	/**
	 * 删除淘宝店铺
	 */
	public function deleteShop($params) {
		if ($this->isNotOnline()) {
			return $this->notOnlineError();
		}
		
		$td = new TaobaoShopDAO();
		return $td->deleteShop($params);
	}
}