/**
 * 售后单-选择销售订单界面
 */
Ext.define("PSI.AfterSales.SelectSOBillForm", {
	extend : "Ext.window.Window",

	config : {
		parentForm : null
	},

	initComponent : function() {
		var me = this;
		Ext.apply(me, {
					title : "选择销售订单",
					modal : true,
					onEsc : Ext.emptyFn,
					width : 800,
					height : 500,
					layout : "border",
					items : [{
								region : "center",
								border : 0,
								bodyPadding : 10,
								layout : "fit",
								items : [me.getSOBillGrid()]
							}, {
								region : "north",
								border : 0,
								layout : {
									type : "table",
									columns : 2
								},
								height : 180,
								bodyPadding : 10,
								items : [{
											html : "<h1>选择销售订单</h1>",
											border : 0,
											colspan : 2
										}, {
											id : "editSORef",
											xtype : "textfield",
											labelAlign : "right",
											labelSeparator : "",
											fieldLabel : "销售订单单号"
										}, {
											xtype : "psi_customerfield",
											id : "editSOCustomer",
											labelAlign : "right",
											labelSeparator : "",
											fieldLabel : "客户"
										}, {
											id : "editFromDT",
											xtype : "datefield",
											format : "Y-m-d",
											labelAlign : "right",
											labelSeparator : "",
											fieldLabel : "业务日期（起）"
										}, {
											id : "editToDT",
											xtype : "datefield",
											format : "Y-m-d",
											labelAlign : "right",
											labelSeparator : "",
											fieldLabel : "业务日期（止）"
										}, {
											xtype : "psi_warehousefield",
											id : "editSOWarehouse",
											labelAlign : "right",
											labelSeparator : "",
											fieldLabel : "仓库"
										}, {
											xtype : "container",
											items : [{
														xtype : "button",
														text : "查询",
														width : 100,
														margin : "0 0 0 10",
														iconCls : "PSI-button-refresh",
														handler : me.onQuery,
														scope : me
													}, {
														xtype : "button",
														text : "清空查询条件",
														width : 100,
														margin : "0, 0, 0, 10",
														handler : me.onClearQuery,
														scope : me
													}]
										}]
							}],
					listeners : {
						show : {
							fn : me.onWndShow,
							scope : me
						}
					},
					buttons : [{
								text : "选择",
								iconCls : "PSI-button-ok",
								formBind : true,
								handler : me.onOK,
								scope : me
							}, {
								text : "取消",
								handler : function() {
									me.close();
								},
								scope : me
							}]
				});

		me.callParent(arguments);
	},

	onWndShow : function() {
		var me = this;
	},

	onOK : function() {
		var me = this;

		var item = me.getSOBillGrid().getSelectionModel().getSelection();
		if (item == null || item.length != 1) {
			PSI.MsgBox.showInfo("请选择销售订单");
			return;
		}
		var bill = item[0];
		me.close();
		me.getParentForm().getSOBillInfo(bill.get("id"));
	},

	getSOBillGrid : function() {
		var me = this;

		if (me.__soBillGrid) {
			return me.__soBillGrid;
		}

		var modelName = "PSISOBill_ASSelectForm";
		Ext.define(modelName, {
					extend : "Ext.data.Model",
					fields : ["id", "ref", "bizDate", "customerName",
							"warehouseName", "inputUserName", "bizUserName",
							"amount"]
				});
		var store = Ext.create("Ext.data.Store", {
					autoLoad : false,
					model : modelName,
					data : [],
					pageSize : 20,
					proxy : {
						type : "ajax",
						actionMethods : {
							read : "POST"
						},
						url : PSI.Const.BASE_URL
								+ "Home/AfterSales/selectSOBillList",
						reader : {
							root : 'dataList',
							totalProperty : 'totalCount'
						}
					}
				});
		store.on("beforeload", function() {
					store.proxy.extraParams = me.getQueryParam();
				});

		me.__soBillGrid = Ext.create("Ext.grid.Panel", {
			columnLines : true,
			columns : [Ext.create("Ext.grid.RowNumberer", {
								text : "序号",
								width : 50
							}), {
						header : "单号",
						dataIndex : "ref",
						width : 110,
						menuDisabled : true,
						sortable : false
					}, {
						header : "业务日期",
						dataIndex : "bizDate",
						menuDisabled : true,
						sortable : false
					}, {
						header : "客户",
						dataIndex : "customerName",
						width : 200,
						menuDisabled : true,
						sortable : false
					}, {
						header : "销售金额",
						dataIndex : "amount",
						menuDisabled : true,
						sortable : false,
						align : "right",
						xtype : "numbercolumn",
						width : 80
					}, {
						header : "出库仓库",
						dataIndex : "warehouseName",
						menuDisabled : true,
						sortable : false
					}, {
						header : "业务员",
						dataIndex : "bizUserName",
						menuDisabled : true,
						sortable : false
					}, {
						header : "录单人",
						dataIndex : "inputUserName",
						menuDisabled : true,
						sortable : false
					}],
			listeners : {
				itemdblclick : {
					fn : me.onOK,
					scope : me
				}
			},
			store : store,
			bbar : [{
						id : "asbill_selectform_pagingToobar",
						xtype : "pagingtoolbar",
						border : 0,
						store : store
					}, "-", {
						xtype : "displayfield",
						value : "每页显示"
					}, {
						id : "asbill_selectform_comboCountPerPage",
						xtype : "combobox",
						editable : false,
						width : 60,
						store : Ext.create("Ext.data.ArrayStore", {
									fields : ["text"],
									data : [["20"], ["50"], ["100"], ["300"],
											["1000"]]
								}),
						value : 20,
						listeners : {
							change : {
								fn : function() {
									store.pageSize = Ext
											.getCmp("asbill_selectform_comboCountPerPage")
											.getValue();
									store.currentPage = 1;
									Ext
											.getCmp("asbill_selectform_pagingToobar")
											.doRefresh();
								},
								scope : me
							}
						}
					}, {
						xtype : "displayfield",
						value : "条记录"
					}]
		});

		return me.__soBillGrid;
	},

	onQuery : function() {
		Ext.getCmp("asbill_selectform_pagingToobar").doRefresh();
	},

	getQueryParam : function() {
		var result = {};

		var ref = Ext.getCmp("editSORef").getValue();
		if (ref) {
			result.ref = ref;
		}

		var customerId = Ext.getCmp("editSOCustomer").getIdValue();
		if (customerId) {
			result.customerId = customerId;
		}

		var warehouseId = Ext.getCmp("editSOWarehouse").getIdValue();
		if (warehouseId) {
			result.warehouseId = warehouseId;
		}

		var fromDT = Ext.getCmp("editFromDT").getValue();
		if (fromDT) {
			result.fromDT = Ext.Date.format(fromDT, "Y-m-d");
		}

		var toDT = Ext.getCmp("editToDT").getValue();
		if (toDT) {
			result.toDT = Ext.Date.format(toDT, "Y-m-d");
		}

		return result;
	},

	onClearQuery : function() {
		var me = this;

		Ext.getCmp("editSORef").setValue(null);
		Ext.getCmp("editSOCustomer").clearIdValue();
		Ext.getCmp("editSOWarehouse").clearIdValue();
		Ext.getCmp("editFromDT").setValue(null);
		Ext.getCmp("editToDT").setValue(null);

		me.onQuery();
	}
});