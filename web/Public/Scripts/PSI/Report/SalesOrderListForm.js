/**
 * 销售订单跟踪表
 */
Ext.define("PSI.Report.SalesOrderListForm", {
	extend : "Ext.panel.Panel",

	border : 0,

	layout : "border",

	initComponent : function() {
		var me = this;

		Ext.apply(me, {
					tbar : [{
								text : "导出Excel",
								iconCls : "PSI-button-excelexport",
								scope : me,
								handler : me.onExcel
							}, "-", {
								text : "关闭",
								iconCls : "PSI-button-exit",
								handler : function() {
									location.replace(PSI.Const.BASE_URL);
								}
							}],
					items : [{
						region : "north",
						height : 120,
						border : 0,
						layout : "fit",
						border : 1,
						title : "查询条件",
						collapsible : true,
						layout : {
							type : "table",
							columns : 4
						},
						items : [{
									id : "editQueryFromDT",
									xtype : "datefield",
									margin : "5, 0, 0, 0",
									format : "Y-m-d",
									labelAlign : "right",
									labelSeparator : "",
									fieldLabel : "订单日期(起)",
									value : null
								}, {
									id : "editQueryToDT",
									xtype : "datefield",
									margin : "5, 0, 0, 0",
									format : "Y-m-d",
									labelAlign : "right",
									labelSeparator : "",
									fieldLabel : "订单日期(止)",
									value : new Date()
								}, {
									id : "editQueryCustomer",
									xtype : "psi_customerfield",
									margin : "5, 0, 0, 0",
									labelAlign : "right",
									labelSeparator : "",
									fieldLabel : "客户"
								}, {
									id : "editQueryGoods",
									xtype : "psi_goodsfield",
									margin : "5, 0, 0, 0",
									labelAlign : "right",
									labelSeparator : "",
									fieldLabel : "商品"
								}, {
									id : "editQueryBizUser",
									xtype : "psi_userfield",
									margin : "5, 0, 0, 0",
									labelAlign : "right",
									labelSeparator : "",
									fieldLabel : "业务员"
								}, {
									id : "editQueryRef",
									xtype : "textfield",
									margin : "5, 0, 0, 0",
									labelAlign : "right",
									labelSeparator : "",
									fieldLabel : "销售订单号"
								}, {
									id : "editQueryDealFromDT",
									xtype : "datefield",
									margin : "5, 0, 0, 0",
									format : "Y-m-d",
									labelAlign : "right",
									labelSeparator : "",
									fieldLabel : "发货日期(起)",
									value : null
								}, {
									id : "editQueryDealToDT",
									xtype : "datefield",
									margin : "5, 0, 0, 0",
									format : "Y-m-d",
									labelAlign : "right",
									labelSeparator : "",
									fieldLabel : "发货日期(止)",
									value : null
								}, {
									id : "editQueryStatus",
									xtype : "combo",
									queryMode : "local",
									editable : false,
									valueField : "id",
									labelAlign : "right",
									labelSeparator : "",
									fieldLabel : "出库状态",
									margin : "5, 0, 0, 0",
									store : Ext.create("Ext.data.ArrayStore", {
												fields : ["id", "text"],
												data : [[-1, "全部"], [0, "未出库"],
														[1000, "已出库"]]
											}),
									value : -1
								}, {
									xtype : "container",
									items : [{
												xtype : "button",
												text : "查询",
												width : 100,
												margin : "5 0 0 10",
												iconCls : "PSI-button-refresh",
												handler : me.onQuery,
												scope : me
											}, {
												xtype : "button",
												text : "重置查询条件",
												width : 100,
												margin : "5, 0, 0, 10",
												handler : me.onClearQuery,
												scope : me
											}]
								}]
					}, {
						region : "center",
						layout : "fit",
						border : 0,
						items : [me.getMainGrid()]
					}]
				});

		me.callParent(arguments);

		var dt = new Date();
		dt.setDate(dt.getDate() - 7);
		Ext.getCmp("editQueryFromDT").setValue(dt);
	},

	getMainGrid : function() {
		var me = this;
		if (me.__mainGrid) {
			return me.__mainGrid;
		}

		var modelName = "PSIReportSalesOrderList";
		Ext.define(modelName, {
					extend : "Ext.data.Model",
					fields : ["goodsCode", "goodsName", "goodsSpec",
							"qcBeginDT", "qcEndDT", "expiration", "unitName",
							"bizDT", "ref", "bizUserName", "customerName",
							"status", "goodsCount", "goodsPrice", "goodsMoney",
							"dealDT"]
				});
		var store = Ext.create("Ext.data.Store", {
					autoLoad : false,
					model : modelName,
					data : [],
					pageSize : 20,
					proxy : {
						type : "ajax",
						actionMethods : {
							read : "POST"
						},
						url : PSI.Const.BASE_URL
								+ "Home/Report/salesOrderListQueryData",
						reader : {
							root : 'dataList',
							totalProperty : 'totalCount'
						}
					}
				});
		store.on("beforeload", function() {
					store.proxy.extraParams = me.getQueryParam();
				});

		me.__mainGrid = Ext.create("Ext.grid.Panel", {
					viewConfig : {
						enableTextSelection : true
					},
					border : 0,
					columnLines : true,
					columns : [{
								xtype : "rownumberer"
							}, {
								header : "商品编码",
								dataIndex : "goodsCode",
								menuDisabled : true,
								sortable : false,
								width : 100
							}, {
								header : "商品名称",
								dataIndex : "goodsName",
								menuDisabled : true,
								sortable : false,
								width : 150
							}, {
								header : "规格型号",
								dataIndex : "goodsSpec",
								menuDisabled : true,
								sortable : false,
								width : 150
							}, {
								header : "生产日期",
								dataIndex : "qcBeginDT",
								menuDisabled : true,
								sortable : false,
								width : 80
							}, {
								header : "保质期(天)",
								dataIndex : "expiration",
								menuDisabled : true,
								sortable : false,
								align : "right",
								width : 80
							}, {
								header : "到期日期",
								dataIndex : "qcEndDT",
								menuDisabled : true,
								sortable : false,
								width : 80
							}, {
								header : "订单日期",
								dataIndex : "bizDT",
								menuDisabled : true,
								sortable : false,
								width : 80
							}, {
								header : "销售订单号",
								dataIndex : "ref",
								menuDisabled : true,
								sortable : false,
								width : 120
							}, {
								header : "业务员",
								dataIndex : "bizUserName",
								menuDisabled : true,
								sortable : false,
								width : 100
							}, {
								header : "客户",
								dataIndex : "customerName",
								menuDisabled : true,
								sortable : false,
								width : 100
							}, {
								header : "出库状态",
								dataIndex : "status",
								menuDisabled : true,
								sortable : false,
								width : 100
							}, {
								header : "销售数量",
								dataIndex : "goodsCount",
								menuDisabled : true,
								sortable : false,
								align : "right",
								xtype : "numbercolumn",
								format : "#"
							}, {
								header : "单位",
								dataIndex : "unitName",
								menuDisabled : true,
								sortable : false,
								width : 60
							}, {
								header : "销售单价",
								dataIndex : "goodsPrice",
								menuDisabled : true,
								sortable : false,
								align : "right",
								xtype : "numbercolumn"
							}, {
								header : "销售金额",
								dataIndex : "goodsMoney",
								menuDisabled : true,
								sortable : false,
								align : "right",
								xtype : "numbercolumn"
							}, {
								header : "发货日期",
								dataIndex : "dealDT",
								menuDisabled : true,
								sortable : false,
								width : 100
							}],
					store : store,
					bbar : [{
								id : "pagingToobar",
								xtype : "pagingtoolbar",
								border : 0,
								store : store
							}, "-", {
								xtype : "displayfield",
								value : "每页显示"
							}, {
								id : "comboCountPerPage",
								xtype : "combobox",
								editable : false,
								width : 60,
								store : Ext.create("Ext.data.ArrayStore", {
											fields : ["text"],
											data : [["20"], ["50"], ["100"],
													["300"], ["1000"]]
										}),
								value : 20,
								listeners : {
									change : {
										fn : function() {
											store.pageSize = Ext
													.getCmp("comboCountPerPage")
													.getValue();
											store.currentPage = 1;
											Ext.getCmp("pagingToobar")
													.doRefresh();
										},
										scope : me
									}
								}
							}, {
								xtype : "displayfield",
								value : "条记录"
							}],
					listeners : {}
				});

		return me.__mainGrid;
	},

	onQuery : function() {
		var me = this;

		me.refreshMainGrid();
	},

	onClearQuery : function() {
		var me = this;

		Ext.getCmp("editQueryFromDT").setValue(null);
		Ext.getCmp("editQueryToDT").setValue(new Date());
		Ext.getCmp("editQueryCustomer").clearIdValue();
		Ext.getCmp("editQueryGoods").clearIdValue();
		Ext.getCmp("editQueryBizUser").clearIdValue();
		Ext.getCmp("editQueryRef").setValue(null);
		Ext.getCmp("editQueryDealFromDT").setValue(null);
		Ext.getCmp("editQueryDealToDT").setValue(null);
		Ext.getCmp("editQueryStatus").setValue(-1);

		me.onQuery();
	},

	getQueryParam : function() {
		var me = this;

		var result = {};

		var fromDT = Ext.getCmp("editQueryFromDT").getValue();
		if (fromDT) {
			result.fromDT = Ext.Date.format(fromDT, "Y-m-d");
		}

		var toDT = Ext.getCmp("editQueryToDT").getValue();
		if (toDT) {
			result.toDT = Ext.Date.format(toDT, "Y-m-d");
		}

		var customerId = Ext.getCmp("editQueryCustomer").getIdValue();
		if (customerId) {
			result.customerId = customerId;
		}

		var goodsId = Ext.getCmp("editQueryGoods").getIdValue();
		if (goodsId) {
			result.goodsId = goodsId;
		}

		var bizUserId = Ext.getCmp("editQueryBizUser").getIdValue();
		if (bizUserId) {
			result.bizUserId = bizUserId;
		}

		var ref = Ext.getCmp("editQueryRef").getValue();
		if (ref) {
			result.ref = ref;
		}

		var dealFromDT = Ext.getCmp("editQueryDealFromDT").getValue();
		if (dealFromDT) {
			result.dealFromDT = Ext.Date.format(dealFromDT, "Y-m-d");
		}

		var dealToDT = Ext.getCmp("editQueryDealToDT").getValue();
		if (dealToDT) {
			result.dealToDT = Ext.Date.format(dealToDT, "Y-m-d");
		}

		var status = Ext.getCmp("editQueryStatus").getValue();
		result.status = status;

		return result;
	},

	refreshMainGrid : function(id) {
		Ext.getCmp("pagingToobar").doRefresh();
	},

	onExcel : function() {
		var me = this;

		var params = me.getQueryParam();

		var url = PSI.Const.BASE_URL + "Home/Report/salesOrderListExcel?json="
				+ Ext.JSON.encode(params);
		window.open(url);
	}
});